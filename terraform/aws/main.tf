terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "josephp"
  region  = var.region
}



resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = "deployer key"
  tags = {
    instance = var.tag_name
  }
  iam_instance_profile = aws_iam_instance_profile.web_instance_profile.name

}
//ssh
resource "aws_key_pair" "deployer" {
  key_name   = "deployer key"
  public_key = file(var.aws_public_key_ssh_path)
}

//VPC
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

//aws_default_security_group pour port 22/80:
resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol    = "tcp"
    self        = true
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    self        = true
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

//creating DB

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name         = "DynamoDB.pradines.EC2.com"
  hash_key     = "UniqID"
  billing_mode   = "PROVISIONED"
  read_capacity  = 10
  write_capacity = 10

  attribute {
    name = "UniqID"
    type = "N"
  }
}

//Policies
resource "aws_iam_role" "ec2_DB_access_role" {
  name               = "ec2_DB_access_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement":[
    {
      "Action": "sts:AssumeRole",
      "Principal": {"Service": "ec2.amazonaws.com"},
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "web_instance_profile" {
    name = "web_instance_profile"
    role = aws_iam_role.ec2_DB_access_role.name
}

resource "aws_iam_role_policy_attachment" "EC2_DynamoDB_FA" {
  role       = aws_iam_role.ec2_DB_access_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

