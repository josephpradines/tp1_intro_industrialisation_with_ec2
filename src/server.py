from flask import Flask
import boto3

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello le monde!"


@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"

@app.route("/db")
def dynamodb():
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('DynamoDB.pradines.EC2.com')
    return "test", table

if __name__ == "__main__":
    # app.run(host="192.168.2.5",port="14292")
    app.run(host="0.0.0.0")
